# -*- mode: ruby -*-
# vi: set ft=ruby :

#создадим переменные
V_GUEST_HOST_NAME = "dev.local"
#рекомендуется использовать 80 порт или зеркало локальный\удаленный
V_GUEST_HOST_PORT = "80"
V_HOST_HOST_PORT = "80"

V_GUEST_SYNCED_FOLDER_PATH = "/work"
V_GUEST_SYNCED_FOLDER_ID = "work"
V_HOST_SYNCED_FOLDER_PATH = "./sync"

V_VM_FRIENDLY_NAME = "skillbox - homework - automatic installation web server drupal and wordpress v3" 
V_VM_MEMORY = "4096"
V_VM_CPU = "4"

V_GUEST_DEPLOY_FOLDER = "/deploy"
V_GUEST_DEPLOY_PATH_WORDPRESS = "wordpress"
V_GUEST_DEPLOY_PATH_DRUPAL = "drupal"

#для работы по имени хоста у вас должна быть dns запись или запись в hosts, с именем хоста и указывающая на ip адрес хостовой машины
V_GUEST_APACHE_SERVER_NAME = "skillbox.local"
V_GUEST_APACHE_ADMIN_MAIL = "admin@dev.local"

V_GUEST_DRUPAL_TITLE = "Skillbox"
V_GUEST_DRUPAL_LOCALE = "ru"
V_GUEST_DRUPAL_ADMIN_USER = "admin"
V_GUEST_DRUPAL_ADMIN_USER_MAIL = "admin@dev.local"

V_GUEST_WORDPRESS_TITLE = "Skillbox"
V_GUEST_WORDPRESS_ADMIN_USER = "admin"
V_GUEST_WORDPRESS_ADMIN_USER_MAIL = "admin@dev.local"

V_GUEST_MYSQL_BACKUP_FOLDER = "backup/mysql"
V_GUEST_MYSQL_BACKUP_KEEP_DAY = "14"

V_GUEST_VAGRANT_DEPLOY_INFO_FILE = "/deploy.txt"

#stage 0
#настраиваем виртуальную машину
Vagrant.configure("2") do |config|
  #ставим ubuntu 20.04 x64
  config.vm.box = "ubuntu/focal64"
  #задаем имя виртуальной машины
  config.vm.hostname = V_GUEST_HOST_NAME
  #указываем порт для проброса с хостовой на виртуальную
  config.vm.network "forwarded_port", guest:V_GUEST_HOST_PORT, host:V_HOST_HOST_PORT
  #задаем mac адрес (нужен для создания vagrant box)
  #config.vm.base_mac = "0800271ECE44"
  #папка, которая будет смонтирована в гостевой системе. путь к папке на хостовой машине берется от корня Vagrant файла
  config.vm.synced_folder V_HOST_SYNCED_FOLDER_PATH, V_GUEST_SYNCED_FOLDER_PATH, id: V_GUEST_SYNCED_FOLDER_ID, automount: true
  #добавляем пользователя vagrant в группу vboxsf для доступа к папке
  config.vm.provision "shell", inline: "usermod -a -G vboxsf vagrant"
  config.vm.boot_timeout = 1800

  config.vm.provider "virtualbox" do |v|
    #понятное имя виртуальной машины в virtualbox
    v.name = V_VM_FRIENDLY_NAME
    #изменить кол-во памяти
    v.memory = V_VM_MEMORY
    #изменить кол-во cpu
    v.cpus = V_VM_CPU
    #тип провайдера виртуализации
    v.customize ["modifyvm", :id, "--paravirtprovider", "kvm"]
    #вначале в vagrant'e надо создать пустой dvd'rom и только потом в него можно смонтировать образ
    v.customize ["storageattach", :id, "--storagectl", "IDE", "--port", "1", "--device", "1", "--type", "dvddrive", "--mtype", "readonly", "--medium", "emptydrive"]
    #смонтируем диск VBoxLinuxAdditions.iso для последующей его установки
    v.customize ["storageattach", :id, "--storagectl", "IDE", "--port", "1", "--device", "1", "--type", "dvddrive", "--mtype", "readonly", "--medium", "additions", "--forceunmount"]
end

#stage 1
#подготавливаем гостевую систему
#устанавливаем обновления и т.д., чистим систему
config.vm.provision :shell, inline: <<-SHELL
  #обновим информацию о пакетах
  sudo apt update
  #обновим пакеты
  sudo apt upgrade -y
  
  #установим VBoxLinuxAdditions
  sudo apt-get install linux-headers-$(uname -r) build-essential dkms -y
  sudo mkdir -p /mnt/cdrom
  sudo mount /dev/cdrom /mnt/cdrom
  cd /mnt/cdrom
  echo y | sudo sh ./VBoxLinuxAdditions.run
  cd /
  eject

  #удалим неиспользуемые пакеты, ненужные зависимости и очистим кэш 
  sudo apt autoclean
  sudo apt clean
  sudo apt autoremove
  sudo apt autoremove --purge
  
  #обновим загрузчик
  sudo update-grub2

  #включим планировщик (cron)
  sudo systemctl enable cron
SHELL

#перезагружаем виртуальную машину
config.vm.provision :shell do |shell|
  shell.privileged = true
  shell.reboot = true
end

#stage 2
#передадим переменные в гостевую систему
#монтируем общую папку в гостевой ос, которая будет доступна даже после перезагрузки vm
config.vm.provision :shell,
  env: {
    "GUEST_SYNCED_FOLDER_PATH" => V_GUEST_SYNCED_FOLDER_PATH,
    "GUEST_SYNCED_FOLDER_ID" => V_GUEST_SYNCED_FOLDER_ID
  },
  inline: <<-SHELL
  sudo rm -rf "$GUEST_SYNCED_FOLDER_PATH"
  sudo ln -sfT "/media/sf_$GUEST_SYNCED_FOLDER_ID" "$GUEST_SYNCED_FOLDER_PATH"
  chown -R vagrant:vagrant "$GUEST_SYNCED_FOLDER_PATH" && chmod -R 777 "$GUEST_SYNCED_FOLDER_PATH"
SHELL

#stage 3
#передадим переменные в гостевую систему
config.vm.provision :shell,
  env: {
    "GUEST_HOST_PORT" => V_GUEST_HOST_PORT,
    "HOST_HOST_PORT" => V_HOST_HOST_PORT,
    "VM_FRIENDLY_NAME" => V_VM_FRIENDLY_NAME,

    "GUEST_SYNCED_FOLDER_PATH" => V_GUEST_SYNCED_FOLDER_PATH,

    "GUEST_DEPLOY_FOLDER" => V_GUEST_DEPLOY_FOLDER,
    "GUEST_DEPLOY_PATH_DRUPAL" => V_GUEST_DEPLOY_PATH_DRUPAL,
    "GUEST_DEPLOY_PATH_WORDPRESS" => V_GUEST_DEPLOY_PATH_WORDPRESS,

    "GUEST_APACHE_SERVER_NAME" => V_GUEST_APACHE_SERVER_NAME,
    "GUEST_APACHE_ADMIN_MAIL" => V_GUEST_APACHE_ADMIN_MAIL,

    "GUEST_DRUPAL_TITLE" => V_GUEST_DRUPAL_TITLE,
    "GUEST_DRUPAL_LOCALE" => V_GUEST_DRUPAL_LOCALE,
    "GUEST_DRUPAL_ADMIN_USER" => V_GUEST_DRUPAL_ADMIN_USER,
    "GUEST_DRUPAL_ADMIN_USER_MAIL" => V_GUEST_DRUPAL_ADMIN_USER_MAIL,

    "GUEST_WORDPRESS_TITLE" => V_GUEST_WORDPRESS_TITLE,
    "GUEST_WORDPRESS_ADMIN_USER" => V_GUEST_WORDPRESS_ADMIN_USER,
    "GUEST_WORDPRESS_ADMIN_USER_MAIL" => V_GUEST_WORDPRESS_ADMIN_USER_MAIL,

    "GUEST_MYSQL_BACKUP_FOLDER" => V_GUEST_MYSQL_BACKUP_FOLDER,
    "GUEST_MYSQL_BACKUP_KEEP_DAY" => V_GUEST_MYSQL_BACKUP_KEEP_DAY,

    "GUEST_VAGRANT_DEPLOY_INFO_FILE" => V_GUEST_VAGRANT_DEPLOY_INFO_FILE
  },
inline: <<-SHELL
  ##################################################################################################################################
  ###создаем уникальные переменные для вэб-сервера, базы данных и сайтов
  #apache2
  myDrupalApache2Pass=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w15 | head -n1)
  myWordpressApache2Pass=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w15 | head -n1)

  #mysql
  #имя пользователя mysql не должно быть больше 32 символов
  #создаем переменные для Drupal
  myDrupalMysqlUser=drupal_user_id_$(tr -cd '[:alnum:]' < /dev/urandom | fold -w12 | head -n1)
  myDrupalMysqlPass=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1)
  myDrupalMysqlDbName=drupal_db_id_$(tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1)

  #создаем переменные для Wordpress
  myWordpressMysqlUser=wp_user_id_$(tr -cd '[:alnum:]' < /dev/urandom | fold -12 | head -n1)
  myWordpressMysqlPass=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1)
  myWordpressMysqlDbName=wp_db_id_$(tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1)
 
  #создаем переменные для резервного копирования баз
  myBackupMysqlUser=backup_user_id_$(tr -cd '[:alnum:]' < /dev/urandom | fold -12 | head -n1)
  myBackupMysqlPass=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1)

  #drupal
  myDrupalWebAdminPassword=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w12 | head -n1)

  #wordpress
  myWordpressWebAdminPassword=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w12 | head -n1)
  ##################################################################################################################################
    
  #создаем временную папку для работы 
  sudo mkdir -p "$GUEST_DEPLOY_FOLDER"

  #создаем папки для резервного копирования баз данных mysql
  sudo mkdir -p "$GUEST_SYNCED_FOLDER_PATH/$GUEST_MYSQL_BACKUP_FOLDER/system"
  sudo mkdir -p "$GUEST_SYNCED_FOLDER_PATH/$GUEST_MYSQL_BACKUP_FOLDER/wordpress"
  sudo mkdir -p "$GUEST_SYNCED_FOLDER_PATH/$GUEST_MYSQL_BACKUP_FOLDER/drupal"

  #установим apache, дополнительные пакеты и включим модуль rewrite
  sudo apt install -y apache2 && sudo apt install -y php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip php-sqlite3 php-cli php-mysql libapache2-mod-php php-apcu && sudo a2enmod rewrite 
  #установим сервер базы данных mysql
  sudo apt install -y mysql-server

  ###подготовка к установке drupal'а
  #из руководства по установке composer
  #создаем файл composer.run для автоматической установки drupal'a
  #экранирование строки, ' ковычка экранирует всю строку, но если есть внутри строки еще одинарная ковычка ', ее не заэкранировать
  #поэтому берем двойные ковычки " и экранируем все спец. символы и другие двойные ковычки, при этом необходимо учитывать уровень вложенности наших команд
  #убил кучу времени на этот вывод в файл, но разобрался (*_*)
  #ссылка с примерами: https://zalinux.ru/?p=1293
  echo '#!/bin/sh' > "$GUEST_DEPLOY_FOLDER/composer.run"
  echo "" >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo "EXPECTED_CHECKSUM=\\"\\$(php -r 'copy\(\\"https://composer.github.io/installer.sig\\", \\"php://stdout\\"\)\;')\\"" >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo "php -r \\"copy('https://getcomposer.org/installer', 'composer-setup.php')\;\\"" >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo "ACTUAL_CHECKSUM=\\"\\$(php -r \\"echo hash_file\('sha384', 'composer-setup.php'\)\;\\")\\"" >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo "" >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo 'if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]' >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo "then" >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo "    >&2 echo 'ERROR: Invalid installer checksum'" >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo "    rm composer-setup.php" >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo "    exit 1" >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo "fi" >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo "" >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo "php composer-setup.php --quiet" >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo 'RESULT=$?' >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo "rm composer-setup.php" >> "$GUEST_DEPLOY_FOLDER/composer.run"
  echo 'exit $RESULT' >> "$GUEST_DEPLOY_FOLDER/composer.run"

  #установим права на выполнение composer.run
  sudo chmod +x "$GUEST_DEPLOY_FOLDER/composer.run"
  #выполним скрипт composer.run
  sudo sh "$GUEST_DEPLOY_FOLDER/composer.run"
  #установим права на выполнение composer.phar
  sudo chmod +x "$GUEST_DEPLOY_FOLDER/composer.phar"
  sudo mv "$GUEST_DEPLOY_FOLDER/composer.phar" /usr/local/bin/composer

  #установим пакет composer
  apt install -y composer
  #скачиваем drupal (актуальный релиз)
  wget -O "$GUEST_DEPLOY_FOLDER/drupal_latest.tar.gz" https://www.drupal.org/download-latest/tar.gz
  #распаковываем архив с drupal (в конечную папку не вариант, так как папка в архиве имеет имя drupal-xxx, конечный путь = переменная, то ее все равно придется перемещать\переименовывать)
  tar -xzf "$GUEST_DEPLOY_FOLDER/drupal_latest.tar.gz" -C "$GUEST_DEPLOY_FOLDER"
  #создаем корневую папку сайта drupal
  sudo mkdir -p "/var/www/$GUEST_DEPLOY_PATH_DRUPAL"
  #перемещяем файлы и папки drupal'а в папку сайта (что перемещать взято из install.txt)
  sudo mv $GUEST_DEPLOY_FOLDER/drupal-*/* $GUEST_DEPLOY_FOLDER/drupal-*/.htaccess $GUEST_DEPLOY_FOLDER/drupal-*/.csslintrc $GUEST_DEPLOY_FOLDER/drupal-*/.editorconfig $GUEST_DEPLOY_FOLDER/drupal-*/.eslintignore $GUEST_DEPLOY_FOLDER/drupal-*/.eslintrc.json $GUEST_DEPLOY_FOLDER/drupal-*/.gitattributes "/var/www/$GUEST_DEPLOY_PATH_DRUPAL"
  #устанавливаем права на папку drupal (пользователь www-data - дефолтный пользователь, под которым запущен php)
  sudo chown -R root:www-data "/var/www/$GUEST_DEPLOY_PATH_DRUPAL/"
  #выставим права на папку drupal, автоматический установщик потом выставит все необходимые права на папки и файлы сам
  sudo chmod -R 777 "/var/www/$GUEST_DEPLOY_PATH_DRUPAL/"

  ###подготовка к установке wordpress'а
  #устанавливаем wp-cli 
  wget -O "$GUEST_DEPLOY_FOLDER/wp-cli.phar" https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
  sudo chmod +x "$GUEST_DEPLOY_FOLDER/wp-cli.phar"
  sudo mv "$GUEST_DEPLOY_FOLDER/wp-cli.phar" "/usr/local/bin/wp-cli"
  #создаем корневую папку сайта wordpress
  sudo mkdir -p "/var/www/$GUEST_DEPLOY_PATH_WORDPRESS"
  #скачиваем wordpress (есть внутреняя проверка файлов)  
  sudo wp-cli core download --allow-root --path="/var/www/$GUEST_DEPLOY_PATH_WORDPRESS"
  #устанавливаем права на папку wordpress (пользователь www-data - дефолтный пользователь, под которым запущен php)
  chown -R root:www-data "/var/www/$GUEST_DEPLOY_PATH_WORDPRESS/"
  
  #удалим за собой
  sudo rm -rf "$GUEST_DEPLOY_FOLDER"

  ##################################################################################################################################
  ###настройка apache2 и mysql
  #настраиваем apache2 на авторизацию
  #загружаем пароли для пользователей Wordpress и Drupal в htpasswd для apache
  echo "$myWordpressApache2Pass" | htpasswd -c -i /etc/apache2/.htpasswd Wordpress
  echo "$myDrupalApache2Pass" | htpasswd -i /etc/apache2/.htpasswd Drupal

  #меняем конфигурационный файл apache2
  echo "<VirtualHost *:$GUEST_HOST_PORT>" > "/etc/apache2/sites-available/000-default.conf"
  echo "ServerAdmin $GUEST_APACHE_ADMIN_MAIL" >> "/etc/apache2/sites-available/000-default.conf"
  echo "ServerName $GUEST_APACHE_SERVER_NAME" >> "/etc/apache2/sites-available/000-default.conf"
  echo 'DocumentRoot /var/www' >> "/etc/apache2/sites-available/000-default.conf"
  echo '' >> "/etc/apache2/sites-available/000-default.conf"
  echo '<Directory /var/www/>' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        AuthType Basic' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        AuthName "Restricted Content"' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        AuthUserFile /etc/apache2/.htpasswd' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        Require valid-user' >> "/etc/apache2/sites-available/000-default.conf"
  echo '' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        #включаем модуль RewriteEngine' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        #при авторизации и совпадении имени удаленного пользователя с "Wordpress"' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        #выполняем редирект в каталог "wordpress" с завершением цикла обработки правил' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        RewriteEngine On' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        RewriteCond %{LA-U:REMOTE_USER} =Wordpress' >> "/etc/apache2/sites-available/000-default.conf"
  echo "        RewriteRule \\"^/?\\$\\" \\"/$GUEST_DEPLOY_PATH_WORDPRESS\\" [R=301,L]" >> "/etc/apache2/sites-available/000-default.conf"
  echo '' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        #при авторизации и совпадении имени удаленного пользователя с "Drupal"' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        #выполняем редирект в каталог "drupal" с завершением цикла обработки правил' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        RewriteCond %{LA-U:REMOTE_USER} =Drupal' >> "/etc/apache2/sites-available/000-default.conf"
  echo "        RewriteRule \\"^/?\\$\\" \\"/$GUEST_DEPLOY_PATH_DRUPAL\\" [R=301,L]" >> "/etc/apache2/sites-available/000-default.conf"
  echo '</Directory>' >> "/etc/apache2/sites-available/000-default.conf"
  echo '' >> "/etc/apache2/sites-available/000-default.conf"
  echo "<Directory /var/www/$GUEST_DEPLOY_PATH_DRUPAL/>" >> "/etc/apache2/sites-available/000-default.conf"
  echo '        Allow from all' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        AllowOverride All' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        Deny from none' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        Options FollowSymlinks' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        Order allow,deny' >> "/etc/apache2/sites-available/000-default.conf"
  echo '' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        AuthType Basic' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        AuthName "Restricted Content"' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        AuthUserFile /etc/apache2/.htpasswd' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        Require user Drupal' >> "/etc/apache2/sites-available/000-default.conf"
  echo '</Directory>' >> "/etc/apache2/sites-available/000-default.conf"
  echo '' >> "/etc/apache2/sites-available/000-default.conf"
  echo "<Directory /var/www/$GUEST_DEPLOY_PATH_WORDPRESS/>" >> "/etc/apache2/sites-available/000-default.conf"
  echo '        Allow from all' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        AllowOverride All' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        Deny from none' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        Options FollowSymlinks' >> "/etc/apache2/sites-available/000-default.conf"
  echo '       Order allow,deny' >> "/etc/apache2/sites-available/000-default.conf"
  echo '' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        AuthType Basic' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        AuthName "Restricted Content"' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        AuthUserFile /etc/apache2/.htpasswd' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        Require user Wordpress' >> "/etc/apache2/sites-available/000-default.conf"
  echo '</Directory>' >> "/etc/apache2/sites-available/000-default.conf"
  echo '' >> "/etc/apache2/sites-available/000-default.conf"
  echo '<Directory /var/www/html>' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        Allow from none' >> "/etc/apache2/sites-available/000-default.conf"
  echo '        Order allow,deny' >> "/etc/apache2/sites-available/000-default.conf"
  echo '</Directory>' >> "/etc/apache2/sites-available/000-default.conf"
  echo '' >> "/etc/apache2/sites-available/000-default.conf"
  echo 'ErrorLog ${APACHE_LOG_DIR}/error.log' >> "/etc/apache2/sites-available/000-default.conf"
  echo 'CustomLog ${APACHE_LOG_DIR}/access.log combined' >> "/etc/apache2/sites-available/000-default.conf"
  echo '</VirtualHost>' >> "/etc/apache2/sites-available/000-default.conf"


  #проверим порт в конфигурационном файле apache2 и добавим его в конфигурационный файл, если там его нет
  if [ $GUEST_HOST_PORT != 80 ] && [ $GUEST_HOST_PORT != 443 ]
  then
    echo "Listen $GUEST_HOST_PORT" >> "/etc/apache2/ports.conf"
  fi
  ##################################################################################################################################


  ##################################################################################################################################
  #создаем пользователей и базы данных mysql
  #создаем пользователя и базу данных для drupal
  mysql -u root -e "create user $myDrupalMysqlUser@'localhost' identified by '$myDrupalMysqlPass';" 
  #создаем базу данных для drupal
  mysql -u root -e "CREATE DATABASE $myDrupalMysqlDbName DEFAULT CHARACTER SET utf8;"
  #разрешаем ему подключение с localhost
  mysql -u root -e "grant all on $myDrupalMysqlDbName.* to $myDrupalMysqlUser@'localhost';"
  mysql -u root -e "flush privileges;"

  #создаем пользователя и базу данных для wordpress
  mysql -u root -e "create user $myWordpressMysqlUser@'localhost' identified by '$myWordpressMysqlPass';"
  #создаем базу данных для wordpress
  mysql -u root -e "CREATE DATABASE $myWordpressMysqlDbName DEFAULT CHARACTER SET utf8;"
  #разрешаем ему подключение с localhost
  mysql -u root -e "grant all on $myWordpressMysqlDbName.* to $myWordpressMysqlUser@'localhost';"
  mysql -u root -e "flush privileges;"

  #создаем пользователя для резервного копирования баз
  mysql -u root -e "CREATE USER $myBackupMysqlUser@'localhost' IDENTIFIED BY '$myBackupMysqlPass';"
  mysql -u root -e "GRANT SELECT, BACKUP_ADMIN, RELOAD, PROCESS, SUPER, REPLICATION CLIENT ON *.* TO $myBackupMysqlUser@'localhost';"
  mysql -u root -e "GRANT CREATE, INSERT, DROP, UPDATE ON mysql.backup_progress TO $myBackupMysqlUser@'localhost';"
  mysql -u root -e "GRANT CREATE, INSERT, DROP, UPDATE, SELECT, ALTER ON mysql.backup_history TO $myBackupMysqlUser@'localhost';"
  mysql -u root -e "GRANT LOCK TABLES, CREATE, DROP, FILE, INSERT, ALTER ON *.* TO $myBackupMysqlUser@'localhost';"
  mysql -u root -e "GRANT CREATE, DROP, UPDATE ON mysql.backup_sbt_history TO $myBackupMysqlUser@'localhost';"
  mysql -u root -e "GRANT ENCRYPTION_KEY_ADMIN ON *.* TO $myBackupMysqlUser@'localhost';"
  mysql -u root -e "GRANT INNODB_REDO_LOG_ARCHIVE ON *.* TO $myBackupMysqlUser@'localhost';"
  mysql -u root -e "flush privileges;"
  ##################################################################################################################################

  ##################################################################################################################################
  ###установка wordpress
  #создаем wp-config.php, соли установщик создает автоматически
  wp-cli config create --allow-root --path="/var/www/$GUEST_DEPLOY_PATH_WORDPRESS" --dbname=$myWordpressMysqlDbName --dbuser=$myWordpressMysqlUser --dbpass=$myWordpressMysqlPass --dbhost=localhost --dbprefix=wp_
  wp-cli config set --allow-root --path="/var/www/$GUEST_DEPLOY_PATH_WORDPRESS" WP_MEMORY_LIMIT 256M
  #устанавливаем wordpress
  if [ $GUEST_HOST_PORT == 80 ]
    then
      sudo wp-cli core install --allow-root --path="/var/www/$GUEST_DEPLOY_PATH_WORDPRESS" --url=http://$GUEST_APACHE_SERVER_NAME/$GUEST_DEPLOY_PATH_WORDPRESS --title=$GUEST_WORDPRESS_TITLE --admin_user=$GUEST_WORDPRESS_ADMIN_USER --admin_password=$myWordpressWebAdminPassword --admin_email=$GUEST_WORDPRESS_ADMIN_USER_MAIL --skip-email
    else
      sudo wp-cli core install --allow-root --path="/var/www/$GUEST_DEPLOY_PATH_WORDPRESS" --url=http://$GUEST_APACHE_SERVER_NAME:$GUEST_HOST_PORT/$GUEST_DEPLOY_PATH_WORDPRESS --title=$GUEST_WORDPRESS_TITLE --admin_user=$GUEST_WORDPRESS_ADMIN_USER --admin_password=$myWordpressWebAdminPassword --admin_email=$GUEST_WORDPRESS_ADMIN_USER_MAIL --skip-email
    fi
  #добавим русский язык
  wp-cli language core install ru_RU --allow-root --path="/var/www/$GUEST_DEPLOY_PATH_WORDPRESS"
  wp-cli language core activate ru_RU --allow-root --path="/var/www/$GUEST_DEPLOY_PATH_WORDPRESS"
  ##################################################################################################################################

  ##################################################################################################################################
  ###установка drupal
  #создаем необходимые папки
  sudo mkdir -p /var/www/$GUEST_DEPLOY_PATH_DRUPAL/sites/default/files/translations
  sudo mkdir -p /var/www/$GUEST_DEPLOY_PATH_DRUPAL/sites/default/files/upload

  #ставим необходиые пакеты
  cd /var/www/$GUEST_DEPLOY_PATH_DRUPAL
  sudo composer update
  sudo composer require drush/drush
  #устанавливаем drupal
  echo y | sudo /var/www/$GUEST_DEPLOY_PATH_DRUPAL/vendor/bin/drush site-install standard --site-name=$GUEST_DRUPAL_TITLE --locale=$GUEST_DRUPAL_LOCALE --site-mail=$GUEST_DRUPAL_ADMIN_USER_MAIL --db-prefix=drupal_ --db-url=mysql://$myDrupalMysqlUser:$myDrupalMysqlPass@localhost:3306/$myDrupalMysqlDbName --account-name=$GUEST_DRUPAL_ADMIN_USER --account-pass=$myDrupalWebAdminPassword --account-mail=$GUEST_DRUPAL_ADMIN_USER_MAIL
  #дополняем конфиг
  echo "ini_set('memory_limit', '256M');" >> /var/www/$GUEST_DEPLOY_PATH_DRUPAL/sites/default/settings.php
  echo "$""settings['file_public_path'] = 'sites/default/files/upload';" >> /var/www/$GUEST_DEPLOY_PATH_DRUPAL/sites/default/settings.php
  chmod a+w /var/www/$GUEST_DEPLOY_PATH_DRUPAL/sites/default/files/upload
  ##################################################################################################################################


  ##################################################################################################################################
  ###cron
  #добавим задачи для резервного копирования mysql, на выходе будет файл вида: имяхоста_autobackup_имябазы_date_дата_time_время.sql.gz
  #задачи будет выполняться каждый день в 1 час ночи
  echo "0 1 * * * /usr/bin/mysqldump --single-transaction -v -h localhost -u $myBackupMysqlUser -p$myBackupMysqlPass sys | gzip > \\"$GUEST_SYNCED_FOLDER_PATH/$GUEST_MYSQL_BACKUP_FOLDER/system/\\`hostname -f\\`_autobackup_sys_date_\\`date \\"+\\%Y_\\%m_\\%d\\"\\`_time_\\`date \\"+\\%H_\\%M\\"\\`.sql.gz\\"" >> /var/spool/cron/crontabs/root
  echo "0 1 * * * /usr/bin/mysqldump --single-transaction -v -h localhost -u $myBackupMysqlUser -p$myBackupMysqlPass mysql | gzip > \\"$GUEST_SYNCED_FOLDER_PATH/$GUEST_MYSQL_BACKUP_FOLDER/system/\\`hostname -f\\`_autobackup_mysql_date_\\`date \\"+\\%Y_\\%m_\\%d\\"\\`_time_\\`date \\"+\\%H_\\%M\\"\\`.sql.gz\\"" >> /var/spool/cron/crontabs/root
  echo "0 1 * * * /usr/bin/mysqldump --single-transaction -v -h localhost -u $myBackupMysqlUser -p$myBackupMysqlPass information_schema | gzip > \\"$GUEST_SYNCED_FOLDER_PATH/$GUEST_MYSQL_BACKUP_FOLDER/system/\\`hostname -f\\`_autobackup_information_schema_date_\\`date \\"+\\%Y_\\%m_\\%d\\"\\`_time_\\`date \\"+\\%H_\\%M\\"\\`.sql.gz\\"" >> /var/spool/cron/crontabs/root
  echo "0 1 * * * /usr/bin/mysqldump --single-transaction -v -h localhost -u $myBackupMysqlUser -p$myBackupMysqlPass performance_schema | gzip > \\"$GUEST_SYNCED_FOLDER_PATH/$GUEST_MYSQL_BACKUP_FOLDER/system/\\`hostname -f\\`_autobackup_performance_schema_date_\\`date \\"+\\%Y_\\%m_\\%d\\"\\`_time_\\`date \\"+\\%H_\\%M\\"\\`.sql.gz\\"" >> /var/spool/cron/crontabs/root
  echo "0 1 * * * /usr/bin/mysqldump --single-transaction -v -h localhost -u $myBackupMysqlUser -p$myBackupMysqlPass $myDrupalMysqlDbName | gzip > \\"$GUEST_SYNCED_FOLDER_PATH/$GUEST_MYSQL_BACKUP_FOLDER/drupal/\\`hostname -f\\`_autobackup_"$myDrupalMysqlDbName"_date_\\`date \\"+\\%Y_\\%m_\\%d\\"\\`_time_\\`date \\"+\\%H_\\%M\\"\\`.sql.gz\\"" >> /var/spool/cron/crontabs/root
  echo "0 1 * * * /usr/bin/mysqldump --single-transaction -v -h localhost -u $myBackupMysqlUser -p$myBackupMysqlPass $myWordpressMysqlDbName | gzip > \\"$GUEST_SYNCED_FOLDER_PATH/$GUEST_MYSQL_BACKUP_FOLDER/wordpress/\\`hostname -f\\`_autobackup_"$myWordpressMysqlDbName"_date_\\`date \\"+\\%Y_\\%m_\\%d\\"\\`_time_\\`date \\"+\\%H_\\%M\\"\\`.sql.gz\\"" >> /var/spool/cron/crontabs/root
  #добавим задачу для удаления старых резервных копий
  #задача будет выполняться каждый день в 3 час ночи
  echo "0 3 * * * find \\"$GUEST_SYNCED_FOLDER_PATH/$GUEST_MYSQL_BACKUP_FOLDER\\" -name '*.gz' -mtime +$GUEST_MYSQL_BACKUP_KEEP_DAY -exec rm -r {} \\;" >> /var/spool/cron/crontabs/root

  #правим права на файл планировщика (cron)
  sudo chown root:root /var/spool/cron/crontabs/root
  sudo chmod 600 /var/spool/cron/crontabs/root
  ##################################################################################################################################

  ##################################################################################################################################
  #выводим файл с информацией
  echo > "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo __________________________________________________________________________________________ >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "$VM_FRIENDLY_NAME" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Available ip address on VM: %ip_address_list%" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  if [ $GUEST_HOST_PORT == 80 ]
    then
      echo "Sites available on localhost: http://localhost" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
      echo "Sites available on URL: http://$GUEST_APACHE_SERVER_NAME" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
    else
      echo "Sites available on localhost: http://localhost:$HOST_HOST_PORT" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
      echo "Sites available on URL: http://$GUEST_APACHE_SERVER_NAME:$HOST_HOST_PORT" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  fi
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Apache user name for Drupal instants: Drupal" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Apache user password for Drupal instants: $myDrupalApache2Pass" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Apache user name for Wordpress instants: Wordpress" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Apache user password for Wordpress instants: $myWordpressApache2Pass" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Drupal admin user name for instants: $GUEST_DRUPAL_ADMIN_USER" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Drupal admin password for instants: $myDrupalWebAdminPassword" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Wordpress admin user name for instants: $GUEST_WORDPRESS_ADMIN_USER" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Wordpress admin password for instants: $myWordpressWebAdminPassword" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Drupal MySQL database name: $myDrupalMysqlDbName" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Drupal MySQL database user name: $myDrupalMysqlUser" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Drupal MySQL database password: $myDrupalMysqlPass" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Wordpress MySQL database name: $myWordpressMysqlDbName" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Wordpress MySQL database user name: $myWordpressMysqlUser" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "Wordpress MySQL database password: $myWordpressMysqlPass" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "MySQL Backup user name: $myBackupMysqlUser" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "MySQL Backup user password: $myBackupMysqlPass" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo "The info file is located: $GUEST_SYNCED_FOLDER_PATH/deploy.txt" >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo __________________________________________________________________________________________ >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  echo >> "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  ##################################################################################################################################
SHELL

#перезагружаем виртуальную машину
config.vm.provision :shell do |shell|                                                                                                    
  shell.privileged = true
  shell.reboot = true
end

#stage 4
#выводим сообщение после окончания работы скрипта
config.vm.provision :shell,
  env: {
    "GUEST_SYNCED_FOLDER_PATH" => V_GUEST_SYNCED_FOLDER_PATH,
    "GUEST_VAGRANT_DEPLOY_INFO_FILE" => V_GUEST_VAGRANT_DEPLOY_INFO_FILE
  },
  inline: <<-SHELL

  sed -i 's/%ip_address_list%/'"$(hostname -I)"'/g' "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  cat "$GUEST_VAGRANT_DEPLOY_INFO_FILE"
  cp "$GUEST_VAGRANT_DEPLOY_INFO_FILE" "$GUEST_SYNCED_FOLDER_PATH"
  SHELL

#stage 5
config.vm.post_up_message = <<-HEREDOC


  Это моя 3 работа по домашке
  Первая работа - https://pastebin.com/yPDGWEfK
  Вторая работа - https://pastebin.com/MV7XR5tR

  Еще можно сделать лучше, но это уже совсем другая задача (-_-)
  Было интересно поизучать возможности Vagrant'a


  P.s. я не linux администратор, но разобраться можно, главное понимать, как сформулировать запрос в поисковике !


HEREDOC
end
