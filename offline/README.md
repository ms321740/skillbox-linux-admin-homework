# Руководство по offline установке Drupal и Wordpress.
## Запуск готового box файла

1.    Скачиваем vagrant box по ссылке:\
	   https://disk.yandex.ru/d/LOfEyPm0wwkH9w
2.    Добавляем vagrant box в свой репозиторий, имя box'а не меняем:\
	   vagrant box add skillbox/linux-admin-homework-offline "%path_to_SkillboxLinuxAdminHomeworkOffline.box%"
3.    Проверяем наличие нашего vagrant box в списке:\
	   vagrant box list\
      Мы должны увидеть строку вида:\
      skillbox/linux-admin-homework-offline (virtualbox, 0)
4.    Создаем папку для нашего проекта\
      Скачиваем в эту папку Vagrantfile 
4.    В этой папке создаем папку с именем "sync"
5.    Проверяем, что 80 порт свободен
6.    Добавляем в DNS запись с именем "skillbox.local"
7.    Запускаем vagrant:\
	   vagrant up
8.    Наша виртуальная машина 2 раза перезагрузится и будет готова к работе
9.    В папке "sync" появится файл "deploy.txt", в нем реквизиты от виртуальной машины\
      В папке "sync" будут создаваться резервные копии базы данных mysql каждый день с максимальной историей в 14 дней.
10.   Не забываем обновить "secret key" у Wordpress'a в его конфигурационном файле и перезапустить вэб сервер командой "systemctl restart apache2":\
      Ключи можно получить одним из следующих способов:\
      С сайта: https://api.wordpress.org/secret-key/1.1/salt/ \
      В консоли: wget -q -O- https://api.wordpress.org/secret-key/1.1/salt/ | grep 'define' \
      Утилитой (обновляет автоматически):\
      wp-cli config shuffle-salts /var/www/wordpress/ --force --allow-root
